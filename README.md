# coincidence_event

This is a Geant4 simulation program for coincicence event(two PMT receive signal at the same time).

## 1.build_fish

Geometry is a seawater ball with 60 m and only a PMT ball.

![img](https://kpcbf4ul2l.feishu.cn/space/api/box/stream/download/asynccode/?code=ZDg3NmZmYjQ0NzVkZGM0ODc3MjcwNjk1YzhmMDJkOGVfZ0t4blhyYmFsRWRTTUNGR3l4RHNhbXlPQlhyVjlZMUdfVG9rZW46Ym94Y244bjJmWHkzOWlBNDJqMnNrZzdmTEVBXzE2NjY2OTIzMzU6MTY2NjY5NTkzNV9WNA)

## 2.build_land_all

Geometry is a seawater ball with 60 m and only a PMT ball and a blocking ball

## 3.build_land_av1

Geometry is a seawater ball with 60 m and only a PMT ball and a blocking ball and fix absorb bug

## 4.build_land_box

Geometry is a seawater box with length 60 m and only a PMT ball and a blocking ball, but cover PMT ball when generate electron and gamma.

## 5.build_land_bv1

Geometry is a seawater box with length 60 m and only a PMT ball and a blocking ball and fix absorb bug but cover PMT ball when generate electron and gamma.

## 6.build_land_bv2

Geometry is a seawater box with length 60 m and only a PMT ball and a blocking ball and fix absorb bug and not cover PMT ball when generate electron and gamma.

![img](https://kpcbf4ul2l.feishu.cn/space/api/box/stream/download/asynccode/?code=YzRhOGMxNjg4OGYzOTJhYjcxNGY2Mjc2MDRhNTI0NTZfdFc5S2M0R1ZLQVphUTFUWmhzcWd2a0phUXpkcGFxVHRfVG9rZW46Ym94Y244dEhVM3E3YzNnRGlaOXFJcjV6Q0p3XzE2NjY2OTIzNDY6MTY2NjY5NTk0Nl9WNA)
