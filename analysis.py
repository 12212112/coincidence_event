# from pdb import post_mortem
import numpy as np
import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
from scipy import interpolate
import matplotlib.colors as colors
from scipy.optimize import curve_fit


'''
function 
'''

def getRealTime(elctronN: int, radius: int):
    '''
    get getRealTime from jingping's result
    '''
    activtyK40 = 10.87
    effective = 0.893
    volume = radius**3
    mass = 1.04 * volume * 10**3
    frequncy = mass * activtyK40 * effective
    time = elctronN / frequncy

    return time


def getActivity(cf,coincidence_num: int, elctronN: int, radius: int):
    '''
    get getActivity from coincidence frequency
    '''
    data_cf = cf
    time = coincidence_num / 3 / data_cf
    effective = 0.893
    volume = radius**3
    mass = 1.04 * volume * 10**3
    all_num = elctronN / effective
    A = all_num / mass / time

    return A


def gauss_fit(x, p, a, sigma2, t0):
    return p + a * np.exp(-(x - t0)**2 / 2 / sigma2)



'''
interpolate quantum efficency 
'''
path_qe = "qe.csv"
data_qe = pd.read_csv(path_qe)

# quantum efficency from YSTC
qe_5 = np.array([0.247,	0.253,	0.259])
qe_17 = np.array([0.236,	0.261,	0.266])
qe_20 = np.array([0.248,	0.245,	0.263])

qe_mean = (qe_5 + qe_17 + qe_20)/3
wl = np.array([405, 445, 483])

# ratio = 100 * (np.sum(qe_5) + np.sum(qe_17) + np.sum(qe_20))/(data_qe["qe"][105] + data_qe["qe"][145] + data_qe["qe"][183])/3

ratio = 1
qe = interpolate.interp1d(1240 / data_qe["wl"], data_qe["qe"] * ratio / 100)

plt.plot(np.linspace(300, 600, 50), qe(1240/np.linspace(300, 600, 50)),label = "XP72B20")

plt.yscale('log')

plt.grid(True, which="both", linestyle='--')
plt.scatter(wl, qe_17,label = "pmt17[USTC]")
plt.scatter(wl, qe_5,label = "pmt5[USTC]")
plt.scatter(wl, qe_20,label = "pmt20[YUTC]")
plt.legend()
plt.xlabel("wavelength[nm]")
plt.ylabel("quantum efficency")
plt.savefig("figure/quantum_efficency.pdf")
plt.show()

########################################################################################################################

'''
read data from pack simulation 
'''
path_data = Path("build_land_bv4")


data_list = []
run_number = 0
name_list = []
for path in path_data.rglob('*.csv'): 
    data_buffer = pd.read_csv(path, skiprows=11, header=None, names=[
                              'event_number', 'time[ns]', 'PMT_num', 'energy[eV]', 'x[mm]', 'y[mm]', 'z[mm]'])
    data_buffer["run_number"] = run_number
    run_number = run_number + 1
    name_list.append(path)
    # print(len(data_buffer))
    data_list.append(data_buffer)

data = pd.concat(data_list, ignore_index=True)

run_times = (run_number) / 2

# print("run_number : ", run_number)
print("totol run times : ", run_times)


'''
fix data
'''
# since in geant4 simulation some value only recorded once in an event
for i in range(len(data['event_number'])):
    if data['event_number'][i] == 0:
        data_copy = data.iloc[i - 1].copy(deep=True)
        data.loc[i, 'event_number'] = data_copy['event_number']
        data.loc[i, 'x[mm]'] = data_copy['x[mm]']
        data.loc[i, 'y[mm]'] = data_copy['y[mm]']
        data.loc[i, 'z[mm]'] = data_copy['z[mm]']


'''
exclude some data(Since in Geant4 program, it have probrability genenrate in DOM)
'''
# dom radius
r = 216
# down PMT ball position(center)
x0, y0, z0 = 0, 0, -28970
# light ball position(center)
x1, y1, z1 = 0, 0, -28110
# up PMT ball position(center)
x2, y2, z2 = 0, 0, -27250
# Hexagonal Buoyancy Module(center)
x3, y3, z3 = 0, 0, -26820
# thickness of Hexagonal Buoyancy Module
h = 300 
# side length of Hexagonal Buoyancy Module
l = 840
data["out box"] = (((data['x[mm]'] - x0)**2 + (data['y[mm]'] - y0)**2 + (data['z[mm]'] - z0)**2)
                   > r**2) & (((data['x[mm]'] - x1)**2 + (data['y[mm]'] - y1)**2 + (data['z[mm]'] - z1)**2) > r**2)


data_rubbish = data[~data["out box"]]
data = data[data["out box"]]

'''
plot K-40 position distribution which emit Cherokov photon received by PMT
'''

data["ρ[mm]"] = np.sqrt(data['x[mm]']**2 + data['y[mm]']**2)


theta = np.arange(0, 2*np.pi, 0.01)
fig = plt.figure()
axes = fig.add_subplot(111)

boxHalfLength = 30000
mm_to_m = 1000
axes.plot((np.sqrt(x0**2 + y0**2) + r * np.cos(theta))/mm_to_m,  ((z0 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "Down PMT Ball")
axes.plot((np.sqrt(x1**2 + y1**2) + r * np.cos(theta))/mm_to_m,  ((z1 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "LED Ball" )
axes.plot((np.sqrt(x2**2 + y2**2) + r * np.cos(theta))/mm_to_m,  ((z2 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "Up PMT Ball")

hh = axes.hist2d(data["ρ[mm]"] / mm_to_m, data['z[mm]']/mm_to_m + boxHalfLength/mm_to_m , bins=(np.linspace(
    0, 4, 50), np.linspace(0, 4, 50)),   norm=colors.LogNorm())

fig.colorbar(hh[3], ax=axes)
axes.hlines((z3 + boxHalfLength + h)/mm_to_m, 0, l/mm_to_m, colors="r")
axes.hlines((z3 + boxHalfLength) /mm_to_m, 0, l/mm_to_m, colors="r")
axes.vlines(l/mm_to_m,(z3 + boxHalfLength) /mm_to_m,(z3 + boxHalfLength + h)/mm_to_m ,colors="r",label = "Hexagonal Buoyancy Module")
plt.legend()
axes.set_xlabel("ρ[m]")
axes.set_ylabel("z[m]")
plt.title("K-40 position distribution(PMTs can recieved photon)")

plt.savefig("figure/K40_distribution_pack.pdf")
plt.savefig("figure/K40_distribution_pack.png")
plt.show()


'''
Print some information
'''
mul_photon = data[data.duplicated(subset=["event_number", "run_number"], keep=False)]

# get coincidence event
ce = mul_photon[~(mul_photon.duplicated(subset=["event_number", "PMT_num"], keep=False))]

coincidence_n = len(ce["event_number"].unique())
print('average coincidence event per run time (no QE)', coincidence_n / run_times)
print('average event per run time (no QE)', len(data) / run_times)
print('average coincidence event / all event (no QE)', coincidence_n / len(data))


print('average coincidence event energy', np.mean(ce['energy[eV]']), 'eV')
print('average event energy', np.mean(data['energy[eV]']), 'eV')
print('average qe: ', np.mean(qe(ce['energy[eV]'])))

print("multi events ", (2 * coincidence_n - len(ce))/coincidence_n)

plt.hist(ce['run_number'], bins=np.linspace(0, run_number-1, run_number), histtype='step')
plt.xlabel("run_number")
plt.ylabel("event_number (no QE)")
plt.show()

plt.hist(ce['run_number']/2, bins=np.linspace(0,
         run_times-1, int(run_times)), histtype='step')
# np.histogram(ce['run_number'],bins =np.linspace(0,run_number-1,run_number))
plt.xlabel("run_times")
plt.ylabel("event_number (no QE)")
plt.show()

theta = np.arange(0, 2*np.pi, 0.01)
fig = plt.figure()
axes = fig.add_subplot(111)

boxHalfLength = 30000
mm_to_m = 1000
axes.plot((np.sqrt(x0**2 + y0**2) + r * np.cos(theta))/mm_to_m,  ((z0 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "Down PMT Ball")
axes.plot((np.sqrt(x1**2 + y1**2) + r * np.cos(theta))/mm_to_m,  ((z1 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "LED Ball" )
axes.plot((np.sqrt(x2**2 + y2**2) + r * np.cos(theta))/mm_to_m,  ((z2 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "Up PMT Ball")

hh = axes.hist2d(ce["ρ[mm]"] / mm_to_m, ce['z[mm]']/mm_to_m + boxHalfLength/mm_to_m , bins=(np.linspace(
    0, 4, 50), np.linspace(0, 4, 50)),   norm=colors.LogNorm())

fig.colorbar(hh[3], ax=axes)
axes.hlines((z3 + boxHalfLength + h)/mm_to_m, 0, l/mm_to_m, colors="r")
axes.hlines((z3 + boxHalfLength) /mm_to_m, 0, l/mm_to_m, colors="r")
axes.vlines(l/mm_to_m,(z3 + boxHalfLength) /mm_to_m,(z3 + boxHalfLength + h)/mm_to_m ,colors="r",label = "Hexagonal Buoyancy Module")
plt.legend()
axes.set_xlabel("ρ[m]")
axes.set_ylabel("z[m]")
plt.title("K-40 position distribution(PMTs can recieved coincidence photon)")

plt.savefig("figure/ce_K40_distribution_pack.pdf")
plt.savefig("figure/ce_K40_distribution_pack.png")
plt.show()

'''
body
'''
loop_num = 100
coincidence_f = []
#data from data analysis
# cf_input = [2.92,2.81,2.79]  #Hz
cf_input = [2.886,2.776,2.73]
A_18_21 = []
A_19_21 = []
A_18_19 = []

ce_list = []
dtimeList = np.array([])

for i in range(loop_num):

    '''
    generate new data after consider quantum efficency
    '''
    data["sample"] = np.random.sample(len(data))
    data['qe'] = data.apply(lambda x: qe(x['energy[eV]']), axis=1)
    data_after_qe = data[data['qe'] > data['sample']]
    data_after_qe = data_after_qe.reset_index()
    '''
    get coincidence event number
    '''

    coincidence_num = 0

    # get mult-event data
    diff_event = np.diff(data_after_qe["event_number"])
    devent = np.where(np.logical_and(diff_event < 90, diff_event > 0))[0]
    # 1 ns ~ 9 events  

    for i in devent:
        if data_after_qe["PMT_num"][i] != data_after_qe["PMT_num"][i+1] and np.abs(data_after_qe["time[ns]"][i] - data_after_qe["time[ns]"][i+1]) < 20:
            coincidence_num += 1
            # print(data_after_qe.loc[i])
            # print(data_after_qe.loc[i + 1])
    
    # get mul-phton data
    data_mul_photon = data_after_qe[data_after_qe.duplicated( subset=["event_number", "run_number"], keep=False)]
    even_list = data_mul_photon["event_number"].unique()
    
    for i in even_list:

        if len(np.unique(list(data_mul_photon[data_mul_photon["event_number"] == i]['PMT_num']))) > 1 and np.max(np.abs(np.diff(data_mul_photon[data_mul_photon["event_number"] == i]['time[ns]']))) < 10 :
  
            coincidence_num += 1
            dtimeList = np.append(dtimeList, np.diff(data_mul_photon[data_mul_photon["event_number"] == i]['time[ns]']))
            # print(data_mul_photon[data_mul_photon["event_number"] == i])

    onceNumber = 250000000

    ce_list.append(coincidence_num)

    A_18_21.append(getActivity(cf_input[0],coincidence_num, onceNumber * run_times, 60))
    A_19_21.append(getActivity(cf_input[1],coincidence_num, onceNumber * run_times, 60))
    A_18_19.append(getActivity(cf_input[2],coincidence_num, onceNumber * run_times, 60))
    
    coincidence_f.append(coincidence_num / 3 /
                         getRealTime(onceNumber * run_times, 60))

# plt.histagram[dtimeList]
plt.hist(dtimeList,bins=np.linspace(-2.5,2.5, 50))
# np.std()
density ,b = np.histogram(dtimeList,bins=np.linspace(-2.5,2.5, 51))

frequncy = density / loop_num/3/ getRealTime(onceNumber * run_times, 60)
time_intergarl=np.linspace(-2.5,2.5, 50)
# param_bounds = ([0, 1, 0, -30], [0.25, 5, 100, -20])
# A, B = curve_fit(gauss_fit, time_intergarl, frequncy, p0=[0.15, 2, 28, -25], bounds=param_bounds)
plt.figure(figsize=(6,4))
plt.scatter(time_intergarl,density/100,c='r',s = 10,alpha=0.65)
plt.xlabel("Time[ns]")
plt.ylabel("count[ns]")
plt.savefig("figure/Cherenkov_pdf.png")
np.std(dtimeList)
plt.show()

print('average coincidence event per run time (after QE)',
      np.sum(ce_list) / run_times / loop_num)
# A = getActivity(np.sum(ce_list), onceNumber * run_times * loop_num, 60)
# coincidence_f = np.sum(ce_list) / 3 / getRealTime(onceNumber * run_times * loop_num, 60)

print("the coincident frequncy of one pair is ", np.mean(coincidence_f), "Hz")
print("sigama is  ", np.std(coincidence_f))

print("the activity is ", np.mean(A_18_21), "Bq/kg")
print("sigama is  ", np.std(A_18_21))

print("the activity is ", np.mean(A_19_21), "Bq/kg")
print("sigama is  ", np.std(A_19_21))

print("the activity is ", np.mean(A_18_19), "Bq/kg")
print("sigama is  ", np.std(A_18_19))


names = ["jingping (3460m)", "jingping(3000m)", "pack_A_18_21","pack_A_19_21","pack_A_18_19"]

result = [10.78178, 10.87285, np.mean(A_18_21),np.mean(A_19_21),np.mean(A_18_19)]

erorr = [0.21456, 0.11693, np.std(A_18_21),np.std(A_19_21),np.std(A_18_19)]

plt.errorbar(names, result, yerr=erorr, fmt='o:', ecolor='hotpink',
             elinewidth=3, ms=5, mfc='wheat', mec='salmon', capsize=3)

plt.ylim(0, 12)
plt.ylabel("Activity(Bq/kg)")
plt.savefig("figure/result_pack.png")
plt.show()

########################################################################################################################

'''
read data from release simulation 
'''
path_data = Path("build_land_bv5")


data_list = []
run_number = 0
name_list = []
for path in path_data.rglob('*.csv'): 
    data_buffer = pd.read_csv(path, skiprows=11, header=None, names=[
                              'event_number', 'time[ns]', 'PMT_num', 'energy[eV]', 'x[mm]', 'y[mm]', 'z[mm]'])
    data_buffer["run_number"] = run_number
    run_number = run_number + 1
    name_list.append(path)
    # print(len(data_buffer))
    data_list.append(data_buffer)

data = pd.concat(data_list, ignore_index=True)

run_times = (run_number) / 2

# print("run_number : ", run_number)
print("totol run times : ", run_times)


'''
fix data
'''
# since in geant4 simulation some value only recorded once in an event
for i in range(len(data['event_number'])):
    if data['event_number'][i] == 0:
        data_copy = data.iloc[i - 1].copy(deep=True)
        data.loc[i, 'event_number'] = data_copy['event_number']
        data.loc[i, 'x[mm]'] = data_copy['x[mm]']
        data.loc[i, 'y[mm]'] = data_copy['y[mm]']
        data.loc[i, 'z[mm]'] = data_copy['z[mm]']


'''
exclude some data(Since in Geant4 program, it have probrability genenrate in DOM)
'''
# dom radius
r = 216
# down PMT ball position(center)
x0, y0, z0 = 0, 0, -28970
# light ball position(center)
x1, y1, z1 = 0, 0, -28110
# up PMT ball position(center)
x2, y2, z2 = 0, 0, -27250
# Hexagonal Buoyancy Module(center)
x3, y3, z3 = 0, 0, -26820
# thickness of Hexagonal Buoyancy Module
h = 300 
# side length of Hexagonal Buoyancy Module
l = 840
data["out box"] = (((data['x[mm]'] - x0)**2 + (data['y[mm]'] - y0)**2 + (data['z[mm]'] - z0)**2)
                   > r**2) & (((data['x[mm]'] - x1)**2 + (data['y[mm]'] - y1)**2 + (data['z[mm]'] - z1)**2) > r**2)


data_rubbish = data[~data["out box"]]
data = data[data["out box"]]

'''
plot K-40 position distribution which emit Cherokov photon received by PMT
'''

data["ρ[mm]"] = np.sqrt(data['x[mm]']**2 + data['y[mm]']**2)


theta = np.arange(0, 2*np.pi, 0.01)
fig = plt.figure()
axes = fig.add_subplot(111)

boxHalfLength = 30000
mm_to_m = 1000
axes.plot((np.sqrt(x0**2 + y0**2) + r * np.cos(theta))/mm_to_m,  ((z0 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "Down PMT Ball")
axes.plot((np.sqrt(x1**2 + y1**2) + r * np.cos(theta))/mm_to_m,  ((z1 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "LED Ball" )
# axes.plot((np.sqrt(x2**2 + y2**2) + r * np.cos(theta))/mm_to_m,  ((z2 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "Up PMT Ball")

hh = axes.hist2d(data["ρ[mm]"] / mm_to_m, data['z[mm]']/mm_to_m + boxHalfLength/mm_to_m , bins=(np.linspace(
    0, 4, 50), np.linspace(0, 4, 50)),   norm=colors.LogNorm())

fig.colorbar(hh[3], ax=axes)
# axes.hlines((z3 + boxHalfLength + h)/mm_to_m, 0, l/mm_to_m, colors="r")
# axes.hlines((z3 + boxHalfLength) /mm_to_m, 0, l/mm_to_m, colors="r")
# axes.vlines(l/mm_to_m,(z3 + boxHalfLength) /mm_to_m,(z3 + boxHalfLength + h)/mm_to_m ,colors="r",label = "Hexagonal Buoyancy Module")
plt.legend()
axes.set_xlabel("ρ[m]")
axes.set_ylabel("z[m]")
plt.title("K-40 position distribution(PMTs can recieved photon)")

plt.savefig("figure/K40_distribution_release.pdf")
plt.savefig("figure/K40_distribution_release.png")
plt.show()


'''
Print some information
'''
mul_photon = data[data.duplicated(subset=["event_number", "run_number"], keep=False)]

# get coincidence event
ce = mul_photon[~(mul_photon.duplicated(subset=["event_number", "PMT_num"], keep=False))]

coincidence_n = len(ce["event_number"].unique())
print('average coincidence event per run time (no QE)', coincidence_n / run_times)
print('average event per run time (no QE)', len(data) / run_times)
print('average coincidence event / all event (no QE)', coincidence_n / len(data))


print('average coincidence event energy', np.mean(ce['energy[eV]']), 'eV')
print('average event energy', np.mean(data['energy[eV]']), 'eV')
print('average qe: ', np.mean(qe(ce['energy[eV]'])))

print("multi events ", (2 * coincidence_n - len(ce))/coincidence_n)

plt.hist(ce['run_number'], bins=np.linspace(0, run_number-1, run_number), histtype='step')
plt.xlabel("run_number")
plt.ylabel("event_number (no QE)")
plt.show()

plt.hist(ce['run_number']/2, bins=np.linspace(0,
         run_times-1, int(run_times)), histtype='step')
# np.histogram(ce['run_number'],bins =np.linspace(0,run_number-1,run_number))
plt.xlabel("run_times")
plt.ylabel("event_number (no QE)")
plt.show()

theta = np.arange(0, 2*np.pi, 0.01)
fig = plt.figure()
axes = fig.add_subplot(111)

boxHalfLength = 30000
mm_to_m = 1000
axes.plot((np.sqrt(x0**2 + y0**2) + r * np.cos(theta))/mm_to_m,  ((z0 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "Down PMT Ball")
axes.plot((np.sqrt(x1**2 + y1**2) + r * np.cos(theta))/mm_to_m,  ((z1 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "LED Ball" )
# axes.plot((np.sqrt(x2**2 + y2**2) + r * np.cos(theta))/mm_to_m,  ((z2 + boxHalfLength) + r * np.sin(theta))/mm_to_m ,label = "Up PMT Ball")

hh = axes.hist2d(ce["ρ[mm]"] / mm_to_m, ce['z[mm]']/mm_to_m + boxHalfLength/mm_to_m , bins=(np.linspace(
    0, 4, 50), np.linspace(0, 4, 50)),   norm=colors.LogNorm())

fig.colorbar(hh[3], ax=axes)
# axes.hlines((z3 + boxHalfLength + h)/mm_to_m, 0, l/mm_to_m, colors="r")
# axes.hlines((z3 + boxHalfLength) /mm_to_m, 0, l/mm_to_m, colors="r")
# axes.vlines(l/mm_to_m,(z3 + boxHalfLength) /mm_to_m,(z3 + boxHalfLength + h)/mm_to_m ,colors="r",label = "Hexagonal Buoyancy Module")
plt.legend()
axes.set_xlabel("ρ[m]")
axes.set_ylabel("z[m]")
plt.title("K-40 position distribution(PMTs can recieved coincidence photon)")

plt.savefig("figure/ce_K40_distribution_release.pdf")
plt.savefig("figure/ce_K40_distribution_release.png")
plt.show()

'''
body
'''
loop_num = 100
coincidence_f = []
#data from data analysis
# cf_input = [2.92,2.81,2.79]  #Hz
A_18_21 = []
A_19_21 = []
A_18_19 = []

ce_list = []

for i in range(loop_num):

    '''
    generate new data after consider quantum efficency
    '''
    data["sample"] = np.random.sample(len(data))
    data['qe'] = data.apply(lambda x: qe(x['energy[eV]']), axis=1)
    data_after_qe = data[data['qe'] > data['sample']]
    data_after_qe = data_after_qe.reset_index()
    '''
    get coincidence event number
    '''

    coincidence_num = 0

    # get mult-event data
    diff_event = np.diff(data_after_qe["event_number"])
    devent = np.where(np.logical_and(diff_event < 90, diff_event > 0))[0]
    # 1 ns ~ 9 events  

    for i in devent:
        if data_after_qe["PMT_num"][i] != data_after_qe["PMT_num"][i+1] and np.abs(data_after_qe["time[ns]"][i] - data_after_qe["time[ns]"][i+1]) < 20:
            coincidence_num += 1
            # print(data_after_qe.loc[i])
            # print(data_after_qe.loc[i + 1])
    
    # get mul-phton data
    data_mul_photon = data_after_qe[data_after_qe.duplicated( subset=["event_number", "run_number"], keep=False)]
    even_list = data_mul_photon["event_number"].unique()
    
    for i in even_list:

        if len(np.unique(list(data_mul_photon[data_mul_photon["event_number"] == i]['PMT_num']))) > 1 and np.max(np.abs(np.diff(data_mul_photon[data_mul_photon["event_number"] == i]['time[ns]']))) < 10 :
  
            coincidence_num += 1
            # dtimeList = np.append(dtimeList, np.diff(data_mul_photon[data_mul_photon["event_number"] == i]['time[ns]']))
            # print(data_mul_photon[data_mul_photon["event_number"] == i])

    onceNumber = 250000000

    ce_list.append(coincidence_num)

    A_18_21.append(getActivity(cf_input[0],coincidence_num, onceNumber * run_times, 60))
    A_19_21.append(getActivity(cf_input[1],coincidence_num, onceNumber * run_times, 60))
    A_18_19.append(getActivity(cf_input[2],coincidence_num, onceNumber * run_times, 60))
    
    coincidence_f.append(coincidence_num / 3 /
                         getRealTime(onceNumber * run_times, 60))

print('average coincidence event per run time (after QE)',
      np.sum(ce_list) / run_times / loop_num)
# A = getActivity(np.sum(ce_list), onceNumber * run_times * loop_num, 60)
# coincidence_f = np.sum(ce_list) / 3 / getRealTime(onceNumber * run_times * loop_num, 60)

print("the coincident frequncy of one pair is ", np.mean(coincidence_f), "Hz")
print("sigama is  ", np.std(coincidence_f))

print("the activity is ", np.mean(A_18_21), "Bq/kg")
print("sigama is  ", np.std(A_18_21))

print("the activity is ", np.mean(A_19_21), "Bq/kg")
print("sigama is  ", np.std(A_19_21))

print("the activity is ", np.mean(A_18_19), "Bq/kg")
print("sigama is  ", np.std(A_18_19))


names = ["jingping (3460m)", "jingping(3000m)", "release_A_18_21","release_A_19_21","release_A_18_19"]

result = [10.78178, 10.87285, np.mean(A_18_21),np.mean(A_19_21),np.mean(A_18_19)]

erorr = [0.21456, 0.11693, np.std(A_18_21),np.std(A_19_21),np.std(A_18_19)]

plt.errorbar(names, result, yerr=erorr, fmt='o:', ecolor='hotpink',
             elinewidth=3, ms=5, mfc='wheat', mec='salmon', capsize=3)

plt.ylim(0, 12)
plt.ylabel("Activity(Bq/kg)")
plt.savefig("figure/result_release.png")
plt.show()

########################################################################################################################
#history

# class mulphotons:

#     def __init__(self, event_number, time, num):
#         self.name = event_number
#         self.time = time
#         self.num = num
#     # # 下面定义了一个say方法
#     # def say(self, content):
#     #     print(content)

#     def judge_ce(self):
#         if len(np.unique(self.num)) == 1:
#             return False
#         else:
#             return True


# celist = []

# ce_list.append(mulphotons(3952720.0, list(data_mul_photon[data_mul_photon["event_number"] == 3952720.0]['time[ns]']), list(
#     data_mul_photon[data_mul_photon["event_number"] == 3952720.0]['PMT_num'])))

# a = mulphotons(3952720.0,
#                list(data_mul_photon[data_mul_photon["event_number"] == 3952720.0]['time[ns]']), list(data_mul_photon[data_mul_photon["event_number"] == 3952720.0]['PMT_num']))

# # .judge_ce()

# ce_list[0].judge_ce()
# list(data_mul_photon[data_mul_photon["event_number"] == 3952720.0]['PMT_num'])
# energy_e=np.array([
# 100 ,
# 150 ,
# 200 ,
# 250 ,
# 300 ,
# 350 ,
# 400 ,
# 450 ,
# 500 ,
# 550 ,
# 600 ,
# 650 ,
# 700 ,
# 750 ,
# 800 ,
# 850 ,
# 900 ,
# 950 ,
# 1000 ,
# 1050 ,
# 1100 ,1310])
# # np.linspace(75,1125,22)
# num_e=np.array([
# 635 ,
# 704 , 
# 771 ,
# 836 ,
# 888 ,
# 932 ,
# 962 ,
# 984 ,
# 995 ,
# 1000 ,
# 996 ,
# 984 ,
# 962 ,
# 936 ,
# 900 ,
# 855 ,
# 796 ,
# 728 ,
# 642 ,
# 549 ,
# 449,0])
# len(num_e)

# plt.hist(num_e, bins= energy_e/100,histtype='step')


# plt.plot(energy_e / 1000, num_e/np.sum(num_e) /0.05)
# plt.vlines(0.265 ,0, 1.2,colors="red",linestyles='dashed',label="Cherenkov threshold energy(0.265MeV) in water")
# plt.xlabel("Energy [MeV]")
# plt.ylabel("Probility/MeV")
# plt.legend()
# plt.title("K-40 Beta decay spectrum")

# plt.savefig("figure/beta_spectrum.jpg")

# plt.figure(figsize=(12,4))
# time_list= np.linspace(-10, 10, 200)

# freq = [
# 5.32,
# 3.80,
# 3.87,
# 7.25,
# 6.72,
# 4.02,
# 4.29,
# 6.41,
# 5.77,
# 4.49]

# # a * np.exp(-(x - t0)**2 / 2 / sigma2
# plt.plot(time_list, freq,color="r")
# # plt.title("frequency from PMT17")
# plt.xlabel("Time [minutes]")
# plt.ylabel("Frequency [kHz]")
# plt.xticks(time_list)
# plt.savefig("figure/dark_noise_PMT.jpg")

# # a * np.exp(-(x - t0)**2 / 2 / sigma2
# plt.plot(time_list, np.ones(100)*0.18,color="r")
# plt.title("coincidence frequency by dark noises")
# plt.xlabel("Δt[ns]")
# plt.ylabel("Frequency [Hz]")
# plt.yticks([])


# plt.savefig("figure/dark_noise_ce.jpg")

# plt.show()

# plt.plot(time_list, np.ones(100)*0.18,color="r")
# plt.title("coincidence frequency by dark noises")
# plt.xlabel("Δt[ns]")
# plt.ylabel("Frequency [Hz]")
# plt.yticks([])


# plt.savefig("figure/dark_noise_ce.jpg")

# plt.show()

# plt.plot(time_list, 1.5 * np.exp(-(time_list)**2 / 2 ),color="r")
# plt.title("coincidence frequency by K-40 light")
# plt.xlabel("Δt[ns]")
# plt.ylabel("Frequency [Hz]")
# plt.yticks([])

# plt.savefig("figure/background light.jpg")

# plt.show()

# n_s_r =  1737.3161764705883 * 136.0

# n_ce_r = 1.015294117647059 * 136.0*10

# n_s_p = 1636.5182481751824 * 136.0

# n_ce_p = 1.004087591240876 * 136.0*10

# retio_s =n_s_p/n_s_r

# sigma_s=retio_s*np.sqrt(1/n_s_p + 1/n_s_r)


# retio_ce =n_ce_p/n_ce_r

# sigma_ce=retio_ce*np.sqrt(1/n_ce_p + 1/n_ce_r)


