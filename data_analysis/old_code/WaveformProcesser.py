import numpy as np

def smooth_wf(volt: np.ndarray, weight=0.25):
    dim = len(volt.shape)
    if dim == 2:
        volt_pre = np.insert(volt[:, :-1], obj=0, values=0., axis=1)
        volt_aft = np.append(volt[:, 1:], values=np.zeros((len(volt), 1)), axis=1)
        volt = (volt + weight*volt_pre + weight*volt_aft) / (1.+2*weight)
    elif dim == 1:
        volt_pre = np.insert(volt[:-1], obj=0, values=0.)
        volt_aft = np.append(volt[1:], values=0.)
        volt = (volt + weight*volt_pre + weight*volt_aft) / (1.+2*weight)
    else:
        print("Error: volt has dim: {dim:d}")
    return volt

def peak_searching(volt: np.ndarray, trig_threshold_low: float,trig_threshold_up: float, empty_l: int = 3, empty_r: int = 5):
    volt_slop = np.heaviside(volt[:, 1:] - volt[:, :-1], 1.)
    volt_curv = volt_slop[:, 1:] - volt_slop[:, :-1]
    mask_curv = volt_curv < 0
    row_false = np.full((len(mask_curv), 1), False)
    mask_curv = np.concatenate([row_false, mask_curv, row_false], axis=1)
    mask_trig_low =volt > trig_threshold_low
    mask_trig_up = volt < trig_threshold_up
    mask = mask_trig_low & mask_trig_up
    mask_peak = mask_curv & mask
    mask_peak[:, :empty_l] = False
    mask_peak[:, -empty_r:] = False
    idx_peak = np.argwhere(mask_peak)
    return idx_peak

def show_waveform(time: np.ndarray, volt: np.ndarray,
                  xmax: float = 1000, ymax: float = 20, 
                  style: str = "scatter") -> None:
    import matplotlib.pyplot as plt
    if style == "scatter":
        plt.scatter(time, volt, s=1)
    elif style == "line":
        plt.plot(time, volt, linewidth=0.7)
    plt.ylim(-1, ymax)
    plt.xlim(0, xmax)
    plt.xlabel("Time [ns]")
    plt.ylabel("Volt [mV]")
    plt.show()


def get_pe(data: np.ndarray, dt: float = 4, resis: float = 50) -> np.ndarray:
    """
    Return PE in 1e6
    """
    ratio = dt / resis / 1.6e-1
    data_pe = np.sum(data, axis=-1) * ratio
    return data_pe

def get_trig_time(data: np.ndarray, trig_th: float) -> np.ndarray:
    trig_adc = np.argmax(data > trig_th, axis=-1)
    trig_adc = trig_adc[trig_adc != 0]
    return trig_adc * 4

def get_wr_noise(data_volt: np.ndarray, dt: float = 4) -> np.ndarray:
    from numpy import fft
    y = np.sum(data_volt, axis=0) / len(data_volt)
    N = len(y)
    T = dt
    yf = fft.rfft(y)
    xf = fft.rfftfreq(N, T)
    wr_band_filter = ((xf > 0.06) & (xf < 0.065)) | ((xf > 0.12) & (xf < 0.13))
    yf[~wr_band_filter] = 0
    return fft.irfft(yf).astype(np.float32)

def get_peak_volt(data_volt: np.ndarray) -> np.ndarray:
    return np.max(data_volt, axis=-1)
