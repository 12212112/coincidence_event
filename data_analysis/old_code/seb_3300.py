from matplotlib import patches
import DataReader
import matplotlib.pyplot as plt
import numpy as np
from scipy import integrate
from scipy.optimize import curve_fit
from pathlib import Path
from WaveformProcesser import peak_searching


dir_data =  Path("fishing_syc")
#file_data = dir_data / "460_8V_8.dat"
channels = [8, 9, 11, 18, 19, 21]
#reader = DataReader.AdcReader(file_data)
time_minutes = 5
def baseline_triger_low(channel: int):
    '''
    返回不同channel的基线加上触发电压。
    '''
    if channel == 8:#PMT 17
        return 79.63341490427653 + 6
    elif channel == 9:#PMT 5
        return 78.46042076746623 + 9
    elif channel == 11:#PMT 20
        return 78.15623839696248 + 6.8
    elif channel == 18:#PMT 39
        return 79.70272143681844 + 4.3
    elif channel == 19:#PMT 9
        return 79.49957370758057 + 6.2
    elif channel == 21:#PMT 29
        return 79.14469989140828 + 2.6
    else:
        print('baseline_triger error')
def baseline_triger_up(channel: int):
    '''
    返回不同channel的基线加上触发电压。
    '''
    if channel == 8:#PMT 17
        return 79.63341490427653 + 30
    elif channel == 9:#PMT 5
        return 78.46042076746623 + 15
    elif channel == 11:#PMT 20
        return 78.15623839696248 + 33.3
    elif channel == 18:#PMT 39
        return 79.70272143681844 + 21.7
    elif channel == 19:#PMT 9
        return 79.49957370758057 + 31
    elif channel == 21:#PMT 29
        return 79.14469989140828 + 7.8
    else:
        print('baseline_triger error')

def time_find(tarray, peakarray):
    '''
    将peakarray返回的相对时间转换为绝对时间。
    '''
    list = []
    for i in range(len(peakarray)):
        list.append(tarray[peakarray[i][0]]+800+4*peakarray[i][1])
    #给定时间序列和peak序列，给出peak的时间分布
    return np.array(list)

def time_array(channel: int):
    '''
    获取触发的时间序列
    '''
    array_time1=np.array([])
    for path in dir_data.rglob('*.dat'):
        reader = DataReader.AdcReader(path)
        volt_array = reader.get_volt(channel)[:, 200:250]  # 得到1000ns中后200ns的数据
        diff_array = np.diff(volt_array[:])                   # 做差
        judge_array = np.amax(diff_array, axis=1) > 3       # 具有上升沿大于3mv的数据索引
        new_varray = volt_array[judge_array]
        new_tarray = reader.get_time(channel)[judge_array]  #以索引筛选时间序列
        array_time1=np.hstack((array_time1,time_find(new_tarray, peak_searching(new_varray,baseline_triger_low(channel),baseline_triger_up(channel)))))
        #peak searching 的阈值为4mV
    return array_time1

def coincidence_frequncy(time_array1, time_array2):
    '''
    给定两个channel的时间序列，给出different time
    '''
    data0 = []
    a_step = np.sort(np.diff(np.sort(np.hstack((time_array1, time_array2)))))
    b_step = np.sort(
        np.diff(np.sort(np.hstack((time_array1+np.int64(1), time_array2)))))
    c_setp = np.subtract(b_step, a_step, dtype='float64')
    new_a = a_step[np.array(abs(c_setp), dtype=bool)]
    new_c = c_setp[np.array(abs(c_setp), dtype=bool)]
    d_step = new_a*new_c
    data0 = np.hstack((data0, d_step))
    return data0
'''
file_data = dir_data / "525_10V4.dat"
reader = DataReader.AdcReader(file_data)
volt_array = reader.get_volt(8)[:, 200:250]  # 得到1000ns中后200ns的数据
diff_array = np.diff(volt_array[:])                   # 做差
judge_array = np.amax(diff_array, axis=1) > 3       # 具有上升沿大于3mv的数据索引
new_varray = volt_array[judge_array]
new_tarray = reader.get_time(8)[judge_array]  #以索引筛选时间序列
time_find(new_tarray,peak_searching(new_varray,baseline_triger(8,4)))
peak_searching(new_varray,baseline_triger(8,4))
array_time1=np.hstack((8,time_find(new_tarray, peak_searching(new_varray,baseline_triger(8,4)))))
'''
data18_19 = coincidence_frequncy(time_array(19), time_array(18))
data18_21 = coincidence_frequncy(time_array(21), time_array(18))
data21_19 = coincidence_frequncy(time_array(21), time_array(19))
data8_9 =  coincidence_frequncy(time_array(8), time_array(9))
data11_9 = coincidence_frequncy(time_array(11), time_array(9))
data11_8 = coincidence_frequncy(time_array(11), time_array(8))


density, b = np.histogram(data21_19, bins=41, range=(-82, 82))
time_intergarl = np.linspace(-82, 82, 41)
frequncy = density/(time_minutes*0.12)


def double_gauss_fit(x, p, a1, D1, t1, a2, D2, t2):
   return p+a1*np.exp(-(x-t1)**2/(2*D1))+a2*np.exp(-(x-t2)**2/(2*D2))


param_bounds = ([0, 1, 100, -25, 1, 0, -25], [1000, 5000, 100000, 25, 5000, 500, 25])
A, B = curve_fit(double_gauss_fit, time_intergarl, frequncy, p0=[
                 500, 500, 800,0,100,100,0], bounds=param_bounds)
'''[0.5,2,8,3],'''
fig=plt.figure(dpi=300,figsize=(12,8))
ax = fig.add_subplot(111)
ax1 = plt.plot(np.linspace(-80, 80, 200), double_gauss_fit(np.linspace(-80,
              80, 200), A[0], A[1], A[2], A[3], A[4], A[5], A[6]), 'r', label="total_fit")
ax2 = plt.plot(np.linspace(-80, 80, 200), double_gauss_fit(np.linspace(-80,
              80, 200), A[0], A[1], A[2], A[3], 0, 0, 0), 'b', label="big_fit", linestyle = "--" )


rect1=patches.Rectangle((-80, 3000), 
                        50, 300, 
                        fc ='none',  
                        ec ='g', 
                        label="small fit parameters",
                        lw = 2)
ax.add_patch(rect1)      
ax.text(-80, 3200,"A = "+str(A[4]))     
ax.text(-80, 3100,"sigma2 = "+str(A[5]))  
ax.text(-80, 3000,"\delta t = "+str(A[6])) 

rect2=patches.Rectangle((-80, 2600), 
                        50, 300, 
                        fc ='none',  
                        ec ='b', 
                        label="big fit parameters",
                        lw = 2)
ax.add_patch(rect2)      
ax.text(-80, 2800,"A = "+str(A[1]))     
ax.text(-80, 2700,"sigma2 = "+str(A[2]))  
ax.text(-80, 2600,"\delta t = "+str(A[3])) 

ax3 = plt.plot(np.linspace(-80, 80, 200), double_gauss_fit(np.linspace(-80,
              80, 200), A[0], 0, 0 ,0, A[4], A[5], A[6]), 'g', label="small_fit", linestyle = "--")

ax4 = plt.scatter(time_intergarl, frequncy, label="experiment_data")
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('single_photon_21_8')

plt.savefig("figures/sin.png")

coincidence_frequncy1 = np.sqrt(A[5]*2*np.pi)*A[4]/4
print("sigma =", np.sqrt(A[5]))
print("a =", A[4])
print("t0 =", A[6])
print("coincidence_frequncy: ", coincidence_frequncy1)
