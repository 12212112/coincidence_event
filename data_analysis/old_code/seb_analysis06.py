from math import e
import DataReader
import matplotlib.pyplot as plt
import numpy as np
from scipy import integrate
from scipy.optimize import curve_fit
from pathlib import Path
from matplotlib import patches
'''
全channel，9分钟的coincidence_frequncy
'''
# 宏变量                     
channels = [8, 9, 11, 18, 19, 21]  # channel number
dir_data = Path("data_land")
time_minutes = 14

######################################################################################
def test_time(time_array: np.array):
    '''
    test a time array is or not Monotonically increasing.
    '''
    boolean_flag = np.subtract(
        time_array[1:], time_array[:-1], dtype="int64") >= 0
    return np.alltrue(boolean_flag)


def out_Tpoint(time_array: np.array):
    '''
    out the turning point of a time array
    '''
    mask = np.subtract(time_array[1:], time_array[:-1], dtype="int64") < 0
    return np.append(mask, [False])


def test_nonEmpty(time_array: np.array):
    '''
    test a time array is or not empty.
    '''
    size_flag = time_array.size
    return size_flag > 0


def out_test(channel1: int):
    '''
    test data and output information.
    '''
    for path in dir_data.rglob('*.dat'):
        Reader = DataReader.AdcReader(path)
        if (not ((test_nonEmpty(Reader.get_time_s(channel1))) & test_nonEmpty(Reader.get_time_adc(channel1)))):
            print(path, "channel:", channel1, "time array is empty")
        else:
            if (not test_time(Reader.get_time_s(channel1))):
                print(path, "channel:", channel1,
                      "time array (second) is not increasing")
            if (not test_time(Reader.get_time_adc(channel1))):
                print(path, "channel:", channel1,
                      "time array (adc) is not increasing")

def out_mask(time_array: np.array):
    '''
    out mask of 3751847<= t <=3751860
    '''
    mask = np.logical_and(time_array<=3751860 , time_array>=3751847)
    return mask

#####################################################################################

file_data = dir_data / "sea_2mV1.dat"
Reader = DataReader.AdcReader(file_data)
mask = out_mask(Reader.get_time_s(18))
Reader.get_time_s(18)[mask]
Reader.get_time(11).size
mask = np.logical_and(Reader.get_time_s(8)<=3751860 , Reader.get_time_s(8)>=3751847)
Reader.get_time_s(8)[mask]
Reader.get_time_adc(8)[mask]

ouuy =Reader.get_time_s(8)[mask]*1e9 + Reader.get_time_adc(8)[mask]*4
test_time(Reader.get_time_adc(8)[mask])

out_test(Reader.get_time_adc(8)[mask])

def coincidence_frequncy(channel1: int, channel2: int):
    data0 = []
    for path in dir_data.rglob('*.dat'):
        Reader = DataReader.AdcReader(path)
        a_step = np.sort(np.diff(
            np.sort(np.hstack((Reader.get_time(channel1), Reader.get_time(channel2))))))
        #
        b_step = np.sort(np.diff(np.sort(np.hstack(
            (Reader.get_time(channel1)+np.int64(1), Reader.get_time(channel2))))))
        c_setp = np.subtract(b_step, a_step, dtype="int64")
        new_a = a_step[np.array(abs(c_setp), dtype=bool)]
        new_c = c_setp[np.array(abs(c_setp), dtype=bool)]
        d_step = new_a * new_c
        data0 = np.hstack((data0, d_step))
    return data0

    
time1 = [0,30,70,1000]
time2 = [10,31,68,100]

data18_19 = coincidence_frequncy(18,19)
data18_21 = coincidence_frequncy(18,21)
data21_19 = coincidence_frequncy(21,19)
data8_9 =  coincidence_frequncy(8,9)
data11_9 = coincidence_frequncy(11,9)
data11_8 = coincidence_frequncy(11,8)

density, b = np.histogram(data18_19, bins=21, range=(-42, 42))
time_intergarl = np.array([-40., -36., -32., -28., -24., -20., -16., -12.,  -8.,  -4.,   0.,
                           4.,   8.,  12.,  16.,  20.,  24.,  28.,  32.,  36.,  40.])
frequncy = density/60/time_minutes


def gauss_fit(x, p, a, sigma2, t0):
    return p+a*np.exp(-(x-t0)**2/2/sigma2)


param_bounds = ([0, 1, 0, -30], [1.7, 5, 100, 30])
A, B = curve_fit(gauss_fit, time_intergarl, frequncy, p0=[
                 0.5, 2, 8, 20], bounds=param_bounds)
'''[0.5,2,8,3],'''

coincidence_frequncy = np.sqrt(A[2]*2*np.pi)*A[1]/4
print("sigma =", np.sqrt(A[2]))
print("a =", A[1])
print("t0 =", A[3])
print("coincidence_frequncy: ", coincidence_frequncy)

fig=plt.figure(dpi=300,figsize=(12,8))
ax = fig.add_subplot(111)
ax1=plt.plot(np.linspace(-40,40,80),gauss_fit(np.linspace(-40,40,80),A[0],A[1],A[2],A[3]),'r',label="fit")
ax2 = plt.scatter(time_intergarl, frequncy, label="experiment_data")

rect2=patches.Rectangle((-35, 2.25), 
                        23, 0.4, 
                        fc ='none',  
                        ec ='b', 
                        label="fit parameters",
                        lw = 0.5)
ax.add_patch(rect2)      
ax.text(-35, 2.6,"A = "+str(A[1]))     
ax.text(-35, 2.5,"sigma2 = "+str(A[2]))  
ax.text(-35, 2.4,"\delta t = "+str(A[3])) 
ax.text(-35, 2.3,"CF = "+str(coincidence_frequncy ))
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('Land_8_9')
plt.savefig("figures/Land_8_9.png")

# def double_gauss_fit(x, p, a1, D1, t1, a2, D2, t2):
#    return p+a1*np.exp(-(x-t1)**2/(2*D1))+a2*np.exp(-(x-t2)**2/(2*D2))

# param_bounds = ([0, 1, 100, -25, 1, 1.5, -25], [1000, 5000, 100000, 25, 5000, 500, 25])
# A, B = curve_fit(double_gauss_fit, time_intergarl, frequncy, p0=[
#                  500, 500, 800,0,100,100,0], bounds=param_bounds)
# '''[0.5,2,8,3],'''
# fig=plt.figure(dpi=300,figsize=(12,8))
# ax = fig.add_subplot(111)
# ax1 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
#               40, 100), A[0], A[1], A[2], A[3], A[4], A[5], A[6]), 'r', label="total_fit")
# ax2 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
#               40, 100), A[0], A[1], A[2], A[3], 0, 0, 0), 'b', label="big_fit", linestyle = "--" )


# rect1=patches.Rectangle((-32, 3.1), 
#                         23, 0.5, 
#                         fc ='none',  
#                         ec ='g', 
#                         label="small fit parameters",
#                         lw = 1)
# ax.add_patch(rect1)      
# ax.text(-32, 3.5,"A = "+str(A[4]))     
# ax.text(-32, 3.4,"sigma2 = "+str(A[5]))  
# ax.text(-32, 3.3,"\delta t = "+str(A[6])) 
# coincidence_frequncy = np.sqrt(A[5]*2*np.pi)*A[4]/4
# ax.text(-32, 3.2,"CF = "+str(coincidence_frequncy ))
# rect2=patches.Rectangle((-32, 2.7), 
#                         23, 0.4, 
#                         fc ='none',  
#                         ec ='b', 
#                         label="big fit parameters",
#                         lw = 1)
# ax.add_patch(rect2)      
# ax.text(-32, 3.0,"A = "+str(A[1]))     
# ax.text(-32, 2.9,"sigma2 = "+str(A[2]))  
# ax.text(-32, 2.8,"\delta t = "+str(A[3])) 

# ax3 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
#               40, 100), A[0], 0, 0 ,0, A[4], A[5], A[6]), 'g', label="small_fit", linestyle = "--")

# ax4 = plt.scatter(time_intergarl, frequncy, label="experiment_data")
# plt.xlabel("\delta t [ns]")
# plt.ylabel("frequncy [Hz]")
# plt.legend(loc='best')
# plt.title('single_photon_9_11')

# plt.savefig("figures/Land_11_9.png")

# wl = np.linspace(300,600,300)
# plt.plot(wl,(200*e**(-wl/45)+3e-6 *e**(wl/53))**-1)

