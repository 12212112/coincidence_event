import numpy as np
from numpy.matrixlib.defmatrix import matrix
import pandas as pd
import uproot


def read_sim(file_data: str, keys: list = None, lib: str = "pd") -> pd.DataFrame:
    with uproot.open(file_data) as file:
        tree = file["ShellHit"]
        if keys:
            df = tree.arrays(keys, library=lib)
        else:
            df = tree.arrays(library=lib)
    return df


class AdcReader:
    def __init__(self, fin_name: str, channel: int = None) -> None:
        with open(fin_name, "rb") as fin:
            data_b = fin.read()
        len_event_write = int.from_bytes(data_b[0:4], "big")
        self.len_event = int((len_event_write+4)/2)  # 4 to account for len_event_write itself
        self.len_event_adc = int.from_bytes(data_b[20:24], "little")
        self.num_event = int(len(data_b)/2/self.len_event)
        # copy will be needed to make ndarray OWNDATA and WRITABLE
        self._data_tot = np.ndarray(shape=(self.num_event, self.len_event),
                                     dtype='<u2', buffer=data_b, order="C").copy()

    def _get_data(self, slice_: slice, dtype_: np.dtype):
        data_ = self._data_tot[:, slice_]
        data_b = data_.tobytes()
        data = np.ndarray(shape=(self.num_event,), dtype=dtype_, buffer=data_b)
        return data.copy()

    def get_sat(self) -> np.ndarray:
        data_adc = self._data_tot[:, 12:12+self.len_event_adc]
        return np.max(data_adc, axis=1) == 4095

    def get_adc(self, channel: int = None) -> np.ndarray:
        data_adc = self._data_tot[:, 12:12+self.len_event_adc]  # only select the adc data
        if channel:
            mask = self.get_channel() == channel
            data_adc = data_adc[mask]
        data_adc &= 0x0fff  # remove the trigger mark
        return data_adc

    def get_volt(self, channel: int = None) -> np.ndarray:
        data_adc = self.get_adc(channel)
        ratio = 1750 / (1 << 12) / 11
        data_volt = np.multiply(data_adc, ratio, dtype=np.float32)
        return data_volt

    @classmethod
    def remove_baseline(cls, data_volt: np.ndarray, num_baseline: int = 50):
        data_baseline = data_volt[:, 0:num_baseline]
        baseline = np.mean(data_baseline, axis=1, dtype=np.float32)
        data_volt -= baseline[:, np.newaxis]
        return data_volt

    def get_channel(self):
        return self._data_tot[:, 2]

    def get_time(self,channel: int = None):
        """
        Return the real time in unit of ns
        """
        time = self.get_time_s(channel)*1e9 + self.get_time_adc(channel)*4
        return time.astype("u8")

    def get_time_adc(self,channel: int = None):
        """
        Return the sub-second time from ADC
        """
        time_adc = self._get_data(slice(6, 8), "<u4")
        if channel:
            mask = self.get_channel() == channel
            time_adc = time_adc[mask]
        return time_adc

    def get_time_s(self,channel: int = None):
        """
        Return the second time from WR
        """
        time_s = self._get_data(slice(8, 10), "<u4")
        if channel:
            mask = self.get_channel() == channel
            time_s = time_s[mask]
        time_s &= 0xffffff
        return time_s

    def get_trigid(self, flag_debug: bool = True):
        trig_id_ = self._data_tot[:, 4:6]
        if flag_debug:
            trig_id_[:, 0] += 1
        trig_id_b = trig_id_.tobytes()
        trig_id = np.ndarray(shape=(self.num_event,), dtype="<u4", buffer=trig_id_b).copy()
        return trig_id


class Event:
    def __init__(self) -> None:
        self.channel_id = 0
        self.trig_id = 0
        self.trig_time_adc = 0
        self.trig_time_wr = 0
        self.data = []
        self.crc0 = 0
        self.crc1 = 0
        pass
    
    @classmethod
    def from_file(cls, fin_name: str) -> list:
        events = []
        fin = open(fin_name, "rb")
        idx = 0
        while(True):
            idx += 1
            if (idx % 10000 == 0):
                print(idx)
            if(fin.read(4) == b""):
                break
            event = Event()
            event.channel_id = int.from_bytes(fin.read(4), "little")
            event.trig_id = int.from_bytes(fin.read(4), "little")
            event.trig_time_adc = int.from_bytes(fin.read(4), "little")
            event.trig_time_wr = int.from_bytes(fin.read(4), "little")
            length = int.from_bytes(fin.read(4), "little")
            for i in range(length):
                event.data.append(int.from_bytes(fin.read(2), "little") & 0x0fff)
            event.crc0 = int.from_bytes(fin.read(4), "little")
            event.crc1 = int.from_bytes(fin.read(4), "little")
            events.append(event)
        print(f"{idx} events read")
        return events

    def __repr__(self) -> str:
        print(f"event ID: {self.trig_id}")
        print(f"channel ID: {self.channel_id}")
        print(f"trigger time: {self.trig_time_wr}: {self.trig_time_adc}*4")
        print(f"data: {self.data}")
        print(f"crc: {self.crc0} + {self.crc1}")
    
    def __str__(self) -> str:
        self.__repr__()
