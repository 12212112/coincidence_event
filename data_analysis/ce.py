import DataReader
import matplotlib.pyplot as plt
import numpy as np
# from scipy import integrate
from scipy.optimize import curve_fit
from scipy import signal
from pathlib import Path
from matplotlib import patches
import pandas as pd
import tqdm
'''
宏变量
'''                     
channels = [8, 9, 11, 18,19, 21]  # channel number
dir_data = Path("data_land")

time_select = [5, 6, 7, 10, 11, 12, 13, 14]
# time_select = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
time_minutes = len(time_select)

'''
functions
'''

def read_data(channel: int):
    data_list = []
    for i in tqdm.tqdm(time_select):
        Reader = DataReader.AdcReader(dir_data / f"sea_2mV{i}.dat")
        data_buffer = pd.DataFrame([Reader.get_time(channel),Reader.get_volt(channel)]).transpose()
        data_rename = data_buffer.rename(columns={0: "time_trigger_2mV[ns]",1: "voltage[mV]"})
        data_list.append(data_rename)
    data = pd.concat(data_list,ignore_index = True)
    return data

def coincidence_event(time_array1, time_array2):
    a_step = np.sort(np.diff(np.sort(np.hstack((time_array1,time_array2)))))
    b_step = np.sort(np.diff(np.sort(np.hstack((time_array1 + np.int64(1), time_array2)))))
    c_step = np.subtract(b_step, a_step, dtype="int64")
    new_a = a_step[np.array(abs(c_step), dtype=bool)]
    new_c = c_step[np.array(abs(c_step), dtype=bool)]
    d_step = new_a * new_c
    return d_step

def gauss_fit(x, p, a, sigma2, t0):
    return p + a * np.exp(-(x - t0)**2 / 2 / sigma2)


def double_gauss_fit(x, p, a1, D1, t1, a2, D2, t2):
   return p + a1 * np.exp(-(x - t1)**2 / (2 * D1)) + a2 * np.exp(-(x - t2)**2/(2 * D2))

'''
get time list
'''

'''
channel 18
'''
dataframe_18 = read_data(18)

# get baseline
dataframe_18["baseline[mV]"] = dataframe_18.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe_18["max voltage[mV]"] = [np.max(dataframe_18["voltage[mV]"][i]) - dataframe_18["baseline[mV]"][i]  for i in range(len(dataframe_18))]

plt.hist(dataframe_18["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.savefig("figures/MaxVoltDist_18.png")
plt.show()

dist = np.histogram(dataframe_18["max voltage[mV]"], bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0])]
dataframe_18["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_18_th = dataframe_18[dataframe_18["max voltage[mV]"]  > dataframe_18["trigger voltage[mV]"]]
data_timelist_18 = np.array([])

for i in tqdm.tqdm(dataframe_18_th.index):
    # hit = (signal.find_peaks(dataframe_18_th["voltage[mV]"][i], height = dataframe_18_th["max voltage[mV]"][i] / 5 + dataframe_18_th["baseline[mV]"][i], distance = 7)[0]) 
    # hit = (signal.find_peaks(dataframe_18_th["voltage[mV]"][i], height = dataframe_18["trigger voltage[mV]"][i] + dataframe_18["baseline[mV]"][i], distance = 7)[0]) 
    hit = (signal.find_peaks(dataframe_18_th["voltage[mV]"][i], height = dataframe_18_th["trigger voltage[mV]"][i] + dataframe_18_th["baseline[mV]"][i], distance = 7)[0]) 

    time_hit = np.append(np.zeros(1)  , 4 * np.diff(hit)) + dataframe_18_th["time_trigger_2mV[ns]"][i]
    data_timelist_18 = np.append(data_timelist_18, time_hit)


print("initial number: ", len(dataframe_18))
print("after trigger number: ", len(dataframe_18_th))
print("after search pluze: ", len(data_timelist_18))


'''
channel 21
'''

dataframe_21 = read_data(21)

# get baseline
dataframe_21["baseline[mV]"] = dataframe_21.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe_21["max voltage[mV]"] = [np.max(dataframe_21["voltage[mV]"][i]) - dataframe_21["baseline[mV]"][i]  for i in range(len(dataframe_21))]

plt.hist(dataframe_21["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.savefig("figures/MaxVoltDist_21.png")
plt.show()

dist = np.histogram(dataframe_21["max voltage[mV]"],bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0])]
dataframe_21["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_21_th = dataframe_21[dataframe_21["max voltage[mV]"]  > dataframe_21["trigger voltage[mV]"]]

data_timelist_21 = np.array([])
for i in tqdm.tqdm(dataframe_21_th.index):
    # hit = (signal.find_peaks(dataframe_21_th["voltage[mV]"][i], height =  dataframe_21_th["max voltage[mV]"][i] / 3 + dataframe_21_th["baseline[mV]"][i], distance = 7)[0]) 
    # hit = (signal.find_peaks(dataframe_21_th["voltage[mV]"][i], height = dataframe_21["trigger voltage[mV]"][i] + dataframe_21["baseline[mV]"][i], distance = 7)[0]) 
    hit = (signal.find_peaks(dataframe_21_th["voltage[mV]"][i], height = dataframe_21_th["trigger voltage[mV]"][i] + dataframe_21_th["baseline[mV]"][i], distance = 7)[0]) 
    
    time_hit = np.append(np.zeros(1)  , 4 * np.diff(hit)) + dataframe_21_th["time_trigger_2mV[ns]"][i]
    data_timelist_21 = np.append(data_timelist_21, time_hit)

print("initial number: ", len(dataframe_21))
print("after trigger number: ", len(dataframe_21_th))
print("after search pluze: ", len(data_timelist_21))

'''
channel 19
'''

dataframe_19 = read_data(19)

# get baseline
dataframe_19["baseline[mV]"] = dataframe_19.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe_19["max voltage[mV]"] = [np.max(dataframe_19["voltage[mV]"][i]) - dataframe_19["baseline[mV]"][i]  for i in range(len(dataframe_19))]

plt.hist(dataframe_19["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.savefig("figures/MaxVoltDist_19.png")
plt.show()

dist = np.histogram(dataframe_19["max voltage[mV]"],bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0][4:])]
dataframe_19["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_19_th = dataframe_19[dataframe_19["max voltage[mV]"]  > dataframe_19["trigger voltage[mV]"]]
data_timelist_19 = np.array([])
for i in tqdm.tqdm(dataframe_19_th.index):
    hit = (signal.find_peaks(dataframe_19_th["voltage[mV]"][i], height = dataframe_19_th["max voltage[mV]"][i] / 3 + dataframe_19_th["baseline[mV]"][i], distance = 7)[0]) 
    # hit = (signal.find_peaks(dataframe_19_th["voltage[mV]"][i], height = dataframe_19["trigger voltage[mV]"][i] + dataframe_19["baseline[mV]"][i], distance = 7)[0]) 
    time_hit = np.append(np.zeros(1)  , 4 *  4 * np.diff(hit)) + dataframe_19_th["time_trigger_2mV[ns]"][i]
    data_timelist_19 = np.append(data_timelist_19, time_hit)

print("initial number: ", len(dataframe_19))
print("after trigger number: ", len(dataframe_19_th))
print("after search pluze: ", len(data_timelist_19))


'''
PMT pairs channnel 18 and 21
'''

# get experiment data frequncy
ce18_21 = coincidence_event(np.array(data_timelist_18, dtype = "int64"), np.array(data_timelist_21, dtype = "int64"))

density, b = np.histogram(ce18_21, bins=21, range=(-42, 42))
# print(density)
time_intergarl = np.linspace(-40,40,21)

frequncy = density / 60 / time_minutes

frequncy_error = np.sqrt(density) /  60 / time_minutes
# fit data
param_bounds = ([0, 1, 0, -15], [0.5, 5, 100, 0])
A, B = curve_fit(gauss_fit, time_intergarl, frequncy, p0=[0.5, 2, 28, -5], bounds=param_bounds)

#print the results
coincidence_frequncy = np.sqrt(A[2] * 2 * np.pi) * A[1] / 4

sigma_ce = np.sqrt(A[2] * 2 * np.pi) * np.sqrt(B[1,1]) / 4 +  np.sqrt(np.pi) * A[1] * np.sqrt(B[2,2])/ (8 * np.sqrt(A[2]))

# B[2,2],B[1,1]
print("sigma =", np.sqrt(A[2]))
print("uncertencty of sigma = ", np.sqrt(B[2,2])/(2*np.sqrt(A[2])) )
print("a =", A[1])
print("t0 =", A[3])
print("coincidence_frequncy: ", coincidence_frequncy)
print("coincidence_frequncy errorbar : ", sigma_ce)

#plot results
fig=plt.figure(dpi=300,figsize=(6,4))
ax = fig.add_subplot(111)
ax1=plt.plot(np.linspace(-40,40,180),gauss_fit(np.linspace(-40,40,180),A[0],A[1],A[2],A[3]),'r',label="best fit")
ax2 = plt.errorbar(time_intergarl, frequncy, yerr= frequncy_error,fmt="o",ecolor="black",color="black", label="data")
plt.xlabel("Δt [ns]")
plt.ylabel("Coincidence Frequency [Hz]")
plt.legend(loc='best')
plt.title('PMT17& PMT20')
plt.savefig("figures/Land_17_20.png")


'''
PMT pairs channels 19 and 21
'''

# get experiment data frequncy
ce19_21 = coincidence_event(np.array(data_timelist_19, dtype = "int64"), np.array(data_timelist_21, dtype = "int64"))

density, b = np.histogram(ce19_21, bins=21, range=(-42, 42))
# print(density)
time_intergarl = np.linspace(-40,40,21)

frequncy = density / 60 / time_minutes

frequncy_error = np.sqrt(density) /  60 / time_minutes
# fit data
param_bounds = ([0, 1, 0, -30], [0.25, 5, 100, -20])
A, B = curve_fit(gauss_fit, time_intergarl, frequncy,p0=[0.15, 2, 28, -25], bounds=param_bounds)

#print the results
coincidence_frequncy = np.sqrt(A[2] * 2 * np.pi) * A[1] / 4

sigma_ce = np.sqrt(A[2] * 2 * np.pi) * np.sqrt(B[1,1]) / 4 +  np.sqrt(np.pi) * A[1] * np.sqrt(B[2,2])/ (8 * np.sqrt(A[2]))

# B[2,2],B[1,1]
print("sigma =", np.sqrt(A[2]))
print("uncertencty of sigma = ", np.sqrt(B[2,2])/(2*np.sqrt(A[2])) )
print("a =", A[1])
print("t0 =", A[3])
print("coincidence_frequncy: ", coincidence_frequncy)
print("coincidence_frequncy errorbar : ", sigma_ce)

#plot results
fig=plt.figure(dpi=300,figsize=(6,4))
ax = fig.add_subplot(111)
ax1=plt.plot(np.linspace(-40,40,180),gauss_fit(np.linspace(-40,40,180),A[0],A[1],A[2],A[3]),'r',label="best fit")
ax2 = plt.errorbar(time_intergarl, frequncy, yerr= frequncy_error,fmt="o",ecolor="black",color="black", label="data")
plt.xlabel("Δt [ns]")
plt.ylabel("Coincidence Frequency [Hz]")
plt.legend(loc='best')
plt.title('PMT5& PMT20')
plt.savefig("figures/Land_5_20.png")

'''
PMT pairs channels 18 and 19
'''
# get experiment data frequncy
ce18_19 = coincidence_event(np.array(data_timelist_18, dtype = "int64"), np.array(data_timelist_19, dtype = "int64"))

density, b = np.histogram(ce18_19, bins=21, range=(-42, 42))
# print(density)
time_intergarl = np.linspace(-40,40,21)

frequncy = density / 60 / time_minutes

frequncy_error = np.sqrt(density) /  60 / time_minutes
# fit data
param_bounds = ([0, 1, 0, 15], [0.5, 5, 100, 25])
A, B = curve_fit(gauss_fit, time_intergarl, frequncy, p0=[0.5, 2, 28, 20], bounds=param_bounds)

#print the results
coincidence_frequncy = np.sqrt(A[2] * 2 * np.pi) * A[1] / 4

sigma_ce = np.sqrt(A[2] * 2 * np.pi) * np.sqrt(B[1,1]) / 4 +  np.sqrt(np.pi) * A[1] * np.sqrt(B[2,2])/ (8 * np.sqrt(A[2]))

# B[2,2],B[1,1]
print("sigma =", np.sqrt(A[2]))
print("uncertencty of sigma = ", np.sqrt(B[2,2])/(2*np.sqrt(A[2])) )
print("a =", A[1])
print("t0 =", A[3])
print("coincidence_frequncy: ", coincidence_frequncy)
print("coincidence_frequncy errorbar : ", sigma_ce)

#plot results
fig=plt.figure(dpi=300,figsize=(6,4))
ax = fig.add_subplot(111)
ax1=plt.plot(np.linspace(-40,40,180),gauss_fit(np.linspace(-40,40,180),A[0],A[1],A[2],A[3]),'r',label="best fit")
ax2 = plt.errorbar(time_intergarl, frequncy, yerr= frequncy_error,fmt="o",ecolor="black",color="black", label="data")
plt.xlabel("Δt [ns]")
plt.ylabel("Coincidence Frequency [Hz]")
plt.legend(loc='best')
plt.title('PMT5& PMT17')
plt.savefig("figures/Land_5_17.png")