import DataReader
import matplotlib.pyplot as plt
import numpy as np
from scipy import integrate
from scipy.optimize import curve_fit
from scipy import signal
from pathlib import Path
from matplotlib import patches
import pandas as pd
import tqdm
'''
宏变量
'''                     
channels = [8, 9, 11, 18, 21]  # channel number
dir_data = Path("data_land")

time_select = [5, 6, 7, 10, 11, 12, 13, 14]
time_minutes = len(time_select)

'''
functions
'''

def read_data(channel: int):
    data_list = []
    for i in time_select:
        Reader = DataReader.AdcReader(dir_data / f"sea_2mV{i}.dat")
        data_buffer = pd.DataFrame([Reader.get_time(channel),Reader.get_volt(channel)]).transpose()
        data_rename = data_buffer.rename(columns={0: "time_trigger_2mV[ns]",1: "voltage[mV]"})
        data_list.append(data_rename)
    data = pd.concat(data_list,ignore_index = True)
    return data

def coincidence_event(time_array1, time_array2):
    a_step = np.sort(np.diff(np.sort(np.hstack((time_array1,time_array2)))))
    b_step = np.sort(np.diff(np.sort(np.hstack((time_array1 + np.int64(1), time_array2)))))
    c_step = np.subtract(b_step, a_step, dtype="int64")
    new_a = a_step[np.array(abs(c_step), dtype=bool)]
    new_c = c_step[np.array(abs(c_step), dtype=bool)]
    d_step = new_a * new_c
    return d_step

def gauss_fit(x, p, a, sigma2, t0):
    return p + a * np.exp(-(x - t0)**2 / 2 / sigma2)


def double_gauss_fit(x, p, a1, D1, t1, a2, D2, t2):
   return p + a1 * np.exp(-(x - t1)**2 / (2 * D1)) + a2 * np.exp(-(x - t2)**2/(2 * D2))

'''
get time list
'''

'''
channel 18
'''
dataframe = read_data(18)

# get baseline
dataframe["baseline[mV]"] = dataframe.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe["max voltage[mV]"] = [np.max(dataframe["voltage[mV]"][i]) - dataframe["baseline[mV]"][i]  for i in range(len(dataframe))]

plt.hist(dataframe["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.show()
plt.savefig("figures/MaxVoltDist_18.png")
dist = np.histogram(dataframe["max voltage[mV]"], bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0])]
dataframe["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_th = dataframe[dataframe["max voltage[mV]"]  > dataframe["trigger voltage[mV]"]]
# dataframe_th.reset_index()

# data_timelist_18 = dataframe_th["time_trigger_2mV[ns]"]
data_timelist_18 = np.array([])
for i in dataframe_th.index:
    hit = (signal.find_peaks(dataframe_th["voltage[mV]"][i], height = dataframe["trigger voltage[mV]"][i] + dataframe["baseline[mV]"][i], distance = 7)[0]) 
    # if len(hit) != 1:
    #     plt.plot(dataframe_th["voltage[mV]"][i])
    #     plt.show()
    time_hit = np.append(np.ones(1), 4 * np.diff(hit)) + dataframe["time_trigger_2mV[ns]"][i]
    data_timelist_18 = np.append(data_timelist_18, time_hit)

print("initial number: ", len(dataframe))
print("after trigger number: ", len(dataframe_th))
print("after search pluze: ", len(data_timelist_18))

'''
channel 21
'''
dataframe = read_data(21)

# get baseline
dataframe["baseline[mV]"] = dataframe.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe["max voltage[mV]"] = [np.max(dataframe["voltage[mV]"][i]) - dataframe["baseline[mV]"][i]  for i in range(len(dataframe))]

plt.hist(dataframe["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.show()
plt.savefig("figures/MaxVoltDist_21.png")
dist = np.histogram(dataframe["max voltage[mV]"],bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0])]
dataframe["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_th = dataframe[dataframe["max voltage[mV]"]  > dataframe["trigger voltage[mV]"]]
# dataframe_th.reset_index()

# data_timelist_21 = dataframe_th["time_trigger_2mV[ns]"]
# data_timelist_21 = np.array([])
# for i in dataframe_th.index:
#     time_hit = (signal.find_peaks(dataframe_th["voltage[mV]"][i], height = dataframe["trigger voltage[mV]"][i] + dataframe["baseline[mV]"][i], distance = 10)[0]) * 4 + dataframe["time_trigger_2mV[ns]"][i]
#     if len(time_hit) == 0:
#         print(i)
#     data_timelist_21 = np.append(data_timelist_21, time_hit)

data_timelist_21 = np.array([])
for i in dataframe_th.index:
    hit = (signal.find_peaks(dataframe_th["voltage[mV]"][i], height = dataframe["trigger voltage[mV]"][i] + dataframe["baseline[mV]"][i], distance = 7)[0]) 
    time_hit = np.append(np.ones(1), 4 * np.diff(hit)) + dataframe["time_trigger_2mV[ns]"][i]
    data_timelist_21 = np.append(data_timelist_21, time_hit)

print("initial number: ", len(dataframe))
print("after trigger number: ", len(dataframe_th))
print("after search pluze: ", len(data_timelist_21))

'''
channel 11
'''
dataframe = read_data(11)

# get baseline
dataframe["baseline[mV]"] = dataframe.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe["max voltage[mV]"] = [np.max(dataframe["voltage[mV]"][i]) - dataframe["baseline[mV]"][i]  for i in range(len(dataframe))]

plt.hist(dataframe["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.show()
plt.savefig("figures/MaxVoltDist_11.png")
dist = np.histogram(dataframe["max voltage[mV]"],bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0])]
dataframe["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_th = dataframe[dataframe["max voltage[mV]"]  > dataframe["trigger voltage[mV]"]]


data_timelist_11 = np.array([])
for i in dataframe_th.index:
    hit = (signal.find_peaks(dataframe_th["voltage[mV]"][i], height = dataframe["trigger voltage[mV]"][i] + dataframe["baseline[mV]"][i], distance = 7)[0]) 
    time_hit = np.append(np.ones(1), 4 * np.diff(hit)) + dataframe["time_trigger_2mV[ns]"][i]
    data_timelist_11 = np.append(data_timelist_11, time_hit)

print("initial number: ", len(dataframe))
print("after trigger number: ", len(dataframe_th))
print("after search pluze: ", len(data_timelist_11))

'''
channel 9
'''
dataframe = read_data(9)

# get baseline
dataframe["baseline[mV]"] = dataframe.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe["max voltage[mV]"] = [np.max(dataframe["voltage[mV]"][i]) - dataframe["baseline[mV]"][i]  for i in range(len(dataframe))]

plt.hist(dataframe["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.show()
plt.savefig("figures/MaxVoltDist_9.png")
dist = np.histogram(dataframe["max voltage[mV]"],bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0])]
dataframe["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_th = dataframe[dataframe["max voltage[mV]"]  > dataframe["trigger voltage[mV]"]]

data_timelist_9 = np.array([])
for i in tqdm.tqdm(dataframe_th.index):
    hit = (signal.find_peaks(dataframe_th["voltage[mV]"][i], height = dataframe["trigger voltage[mV]"][i] + dataframe["baseline[mV]"][i], distance = 7)[0]) 
    # if len(hit) == 0 :
    #    print(i)
    time_hit = np.append(np.ones(1), 4 * np.diff(hit)) + dataframe["time_trigger_2mV[ns]"][i]
    data_timelist_9 = np.append(data_timelist_9, time_hit)

print("initial number: ", len(dataframe))
print("after trigger number: ", len(dataframe_th))
print("after search pluze: ", len(data_timelist_9))

# plt.plot(dataframe_th["voltage[mV]"][2007032])
# plt.show()
'''
channel 8
'''
dataframe = read_data(8)

# get baseline
dataframe["baseline[mV]"] = dataframe.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe["max voltage[mV]"] = [np.max(dataframe["voltage[mV]"][i]) - dataframe["baseline[mV]"][i]  for i in range(len(dataframe))]

plt.hist(dataframe["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.show()
plt.savefig("figures/MaxVoltDist_8.png")
dist = np.histogram(dataframe["max voltage[mV]"],bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0])]
dataframe["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_th = dataframe[dataframe["max voltage[mV]"]  > dataframe["trigger voltage[mV]"]]


data_timelist_8 = np.array([])
for i in dataframe_th.index:
    hit = (signal.find_peaks(dataframe_th["voltage[mV]"][i], height = dataframe["trigger voltage[mV]"][i] + dataframe["baseline[mV]"][i], distance = 7)[0]) 
    time_hit = np.append(np.ones(1), 4 * np.diff(hit)) + dataframe["time_trigger_2mV[ns]"][i]
    data_timelist_8 = np.append(data_timelist_8, time_hit)

print("initial number: ", len(dataframe))
print("after trigger number: ", len(dataframe_th))
print("after search pluze: ", len(data_timelist_8))

'''
channel 19
'''
dataframe = read_data(19)

# get baseline
dataframe["baseline[mV]"] = dataframe.apply(lambda x: np.mean(x["voltage[mV]"][0:10]), axis = 1)

# get max voltage (subtract baseline)
dataframe["max voltage[mV]"] = [np.max(dataframe["voltage[mV]"][i]) - dataframe["baseline[mV]"][i]  for i in range(len(dataframe))]

plt.hist(dataframe["max voltage[mV]"], bins= 200)
plt.xlabel("voltage[mV]")
plt.ylabel("count")
plt.show()
plt.savefig("figures/MaxVoltDist_19.png")
dist = np.histogram(dataframe["max voltage[mV]"],bins= 200)

# get trigger voltage
sPE = dist[1][np.argmax(dist[0][4:])]
dataframe["trigger voltage[mV]"] = sPE / 4
print("sPE = ", sPE ,"mV")

# select photon wave higer than trigger
dataframe_th = dataframe[dataframe["max voltage[mV]"]  > dataframe["trigger voltage[mV]"]]

data_timelist_19 = np.array([])
for i in tqdm.tqdm(dataframe_th.index):
    hit = (signal.find_peaks(dataframe_th["voltage[mV]"][i], height = dataframe["trigger voltage[mV]"][i] + dataframe["baseline[mV]"][i], distance = 7)[0]) 
    time_hit = np.append(np.ones(1), 4 * np.diff(hit)) + dataframe["time_trigger_2mV[ns]"][i]
    data_timelist_19 = np.append(data_timelist_19, time_hit)

print("initial number: ", len(dataframe))
print("after trigger number: ", len(dataframe_th))
print("after search pluze: ", len(data_timelist_19))


'''
PMT pairs 18 and 21
'''

# get experiment data frequncy
ce18_21 = coincidence_event(np.array(data_timelist_18, dtype = "int64"), np.array(data_timelist_21, dtype = "int64"))

density, b = np.histogram(ce18_21, bins=21, range=(-42, 42))
# print(density)
time_intergarl = np.array([-40., -36., -32., -28., -24., -20., -16., -12.,  -8.,  -4.,   0.,
                           4.,   8.,  12.,  16.,  20.,  24.,  28.,  32.,  36.,  40.])
frequncy = density / 60 / time_minutes

# fit data
param_bounds = ([0, 1, 0, -15], [0.5, 5, 100, 0])
A, B = curve_fit(gauss_fit, time_intergarl, frequncy, p0=[0.5, 2, 28, -5], bounds=param_bounds)

#print the results
coincidence_frequncy = np.sqrt(A[2] * 2 * np.pi) * A[1] / 4
print("sigma =", np.sqrt(A[2]))
print("a =", A[1])
print("t0 =", A[3])
print("coincidence_frequncy: ", coincidence_frequncy)

#plot results
fig=plt.figure(dpi=300,figsize=(12,8))
ax = fig.add_subplot(111)
ax1=plt.plot(np.linspace(-40,40,80),gauss_fit(np.linspace(-40,40,80),A[0],A[1],A[2],A[3]),'r',label="fit")
ax2 = plt.scatter(time_intergarl, frequncy, label="experiment_data")

rect2=patches.Rectangle((-35, 2.25), 
                        23, 0.4, 
                        fc ='none',  
                        ec ='b', 
                        label="fit parameters",
                        lw = 0.5)
ax.add_patch(rect2)      
ax.text(-35, 2.6,"A = "+ str('%f' %A[1]))     
ax.text(-35, 2.5,"sigma2 = "+str('%f' %A[2]))  
ax.text(-35, 2.4,"\delta t = "+str('%f' %A[3])) 
ax.text(-35, 2.3,"CF = "+str('%f' %coincidence_frequncy ))
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('Land_18_21')
plt.savefig("figures/Land_18_21.png")






'''
PMT pairs 19 and 21
'''

# get experiment data frequncy
ce19_21 = coincidence_event(np.array(data_timelist_19, dtype = "int64"), np.array(data_timelist_21, dtype = "int64"))

density, b = np.histogram(ce19_21, bins=21, range=(-42, 42))
# print(density)
time_intergarl = np.array([-40., -36., -32., -28., -24., -20., -16., -12.,  -8.,  -4.,   0.,
                           4.,   8.,  12.,  16.,  20.,  24.,  28.,  32.,  36.,  40.])
frequncy = density / 60 / time_minutes

# fit data
param_bounds = ([0, 1, 0, -30], [0.25, 5, 100, -20])
A, B = curve_fit(gauss_fit, time_intergarl, frequncy, p0=[0.15, 2, 28, -25], bounds=param_bounds)

#print the results
coincidence_frequncy = np.sqrt(A[2] * 2 * np.pi) * A[1] / 4
print("sigma =", np.sqrt(A[2]))
print("a =", A[1])
print("t0 =", A[3])
print("coincidence_frequncy: ", coincidence_frequncy)

#plot results
fig=plt.figure(dpi=300,figsize=(12,8))
ax = fig.add_subplot(111)
ax1=plt.plot(np.linspace(-40,40,80),gauss_fit(np.linspace(-40,40,80),A[0],A[1],A[2],A[3]),'r',label="fit")
ax2 = plt.scatter(time_intergarl, frequncy, label="experiment_data")

rect2=patches.Rectangle((-35, 2.25), 
                        23, 0.4, 
                        fc ='none',  
                        ec ='b', 
                        label="fit parameters",
                        lw = 0.5)
ax.add_patch(rect2)      
ax.text(-35, 2.6,"A = "+ str('%f' %A[1]))     
ax.text(-35, 2.5,"sigma2 = "+str('%f' %A[2]))  
ax.text(-35, 2.4,"\delta t = "+str('%f' %A[3])) 
ax.text(-35, 2.3,"CF = "+str('%f' %coincidence_frequncy ))
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('Land_19_21')
plt.savefig("figures/Land_19_21.png")




'''
PMT pairs 18 and 19
'''

# get experiment data frequncy
ce18_19 = coincidence_event(np.array(data_timelist_18, dtype = "int64"), np.array(data_timelist_19, dtype = "int64"))

density, b = np.histogram(ce18_19, bins=21, range=(-42, 42))
# print(density)
time_intergarl = np.array([-40., -36., -32., -28., -24., -20., -16., -12.,  -8.,  -4.,   0.,
                           4.,   8.,  12.,  16.,  20.,  24.,  28.,  32.,  36.,  40.])
frequncy = density / 60 / time_minutes

# fit data
param_bounds = ([0, 1, 0, 15], [0.5, 5, 100, 25])
A, B = curve_fit(gauss_fit, time_intergarl, frequncy, p0=[0.5, 2, 28, 20], bounds=param_bounds)

#print the results
coincidence_frequncy = np.sqrt(A[2] * 2 * np.pi) * A[1] / 4
print("sigma =", np.sqrt(A[2]))
print("a =", A[1])
print("t0 =", A[3])
print("coincidence_frequncy: ", coincidence_frequncy)

#plot results
fig=plt.figure(dpi=300,figsize=(12,8))
ax = fig.add_subplot(111)
ax1=plt.plot(np.linspace(-40,40,80),gauss_fit(np.linspace(-40,40,80),A[0],A[1],A[2],A[3]),'r',label="fit")
ax2 = plt.scatter(time_intergarl, frequncy, label="experiment_data")

rect2=patches.Rectangle((-35, 2.25), 
                        23, 0.4, 
                        fc ='none',  
                        ec ='b', 
                        label="fit parameters",
                        lw = 0.5)
ax.add_patch(rect2)      
ax.text(-35, 2.6,"A = "+ str('%f' %A[1]))     
ax.text(-35, 2.5,"sigma2 = "+str('%f' %A[2]))  
ax.text(-35, 2.4,"\delta t = "+str('%f' %A[3])) 
ax.text(-35, 2.3,"CF = "+str('%f' %coincidence_frequncy ))
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('Land_18_19')
plt.savefig("figures/Land_18_19.png")



'''
PMT pairs 8 and 11
'''

# get experiment data frequncy
ce8_11 = coincidence_event(np.array(data_timelist_8, dtype = "int64"), np.array(data_timelist_11, dtype = "int64"))

density, b = np.histogram(ce8_11, bins=21, range=(-42, 42))
time_intergarl = np.array([-40., -36., -32., -28., -24., -20., -16., -12.,  -8.,  -4.,   0.,
                           4.,   8.,  12.,  16.,  20.,  24.,  28.,  32.,  36.,  40.])
frequncy = density/ 60 /time_minutes


param_bounds = ([0, 1, 100, -25, 1, 1.5, -25], [1000, 5000, 100000, 25, 5000, 500, 25])
A, B = curve_fit(double_gauss_fit, time_intergarl, frequncy, p0=[
                 500, 500, 800,0,100,100,0], bounds=param_bounds)
'''[0.5,2,8,3],'''
fig=plt.figure(dpi=300,figsize=(12,8))
ax = fig.add_subplot(111)
ax1 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], A[1], A[2], A[3], A[4], A[5], A[6]), 'r', label="total_fit")
ax2 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], A[1], A[2], A[3], 0, 0, 0), 'b', label="big_fit", linestyle = "--" )


rect1=patches.Rectangle((-32, 3.1), 
                        23, 0.5, 
                        fc ='none',  
                        ec ='g', 
                        label="small fit parameters",
                        lw = 1)
ax.add_patch(rect1)      
ax.text(-32, 3.5,"A = " + str('%f' %A[4]))     
ax.text(-32, 3.4,"sigma2 = " + str('%f' %A[5]))  
ax.text(-32, 3.3,"\delta t = " + str('%f' %A[6]))
coincidence_frequncy = np.sqrt(A[5]*2*np.pi)*A[4]/4
ax.text(-32, 3.2,"CF = "+str('%f' %coincidence_frequncy ))
rect2=patches.Rectangle((-32, 2.7), 
                        23, 0.4, 
                        fc ='none',  
                        ec ='b', 
                        label="big fit parameters",
                        lw = 1)
ax.add_patch(rect2)      
ax.text(-32, 3.0,"A = "+str('%f' %A[1]))     
ax.text(-32, 2.9,"sigma2 = "+str('%f' %A[2]))  
ax.text(-32, 2.8,"\delta t = "+str('%f' %A[3])) 

ax3 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], 0, 0 ,0, A[4], A[5], A[6]), 'g', label="small_fit", linestyle = "--")

ax4 = plt.scatter(time_intergarl, frequncy, label="experiment_data")
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('Land_8_11')

plt.savefig("figures/Land_8_11.png")



'''
PMT pairs 8 and 9
'''

# get experiment data frequncy
ce8_9 = coincidence_event(np.array(data_timelist_8, dtype = "int64"), np.array(data_timelist_9, dtype = "int64"))

density, b = np.histogram(ce8_9, bins=21, range=(-42, 42))
time_intergarl = np.array([-40., -36., -32., -28., -24., -20., -16., -12.,  -8.,  -4.,   0.,
                           4.,   8.,  12.,  16.,  20.,  24.,  28.,  32.,  36.,  40.])
frequncy = density/ 60 /time_minutes


param_bounds = ([0, 1, 100, -25, 1, 1.5, -25], [1000, 5000, 100000, 25, 5000, 500, 25])
A, B = curve_fit(double_gauss_fit, time_intergarl, frequncy, p0=[
                 500, 500, 800,0,100,100,0], bounds=param_bounds)
'''[0.5,2,8,3],'''
fig=plt.figure(dpi=300,figsize=(12,8))
ax = fig.add_subplot(111)
ax1 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], A[1], A[2], A[3], A[4], A[5], A[6]), 'r', label="total_fit")
ax2 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], A[1], A[2], A[3], 0, 0, 0), 'b', label="big_fit", linestyle = "--" )


rect1=patches.Rectangle((-32, 3.1), 
                        23, 0.5, 
                        fc ='none',  
                        ec ='g', 
                        label="small fit parameters",
                        lw = 1)
ax.add_patch(rect1)      
ax.text(-32, 3.5,"A = " + str('%f' %A[4]))     
ax.text(-32, 3.4,"sigma2 = " + str('%f' %A[5]))  
ax.text(-32, 3.3,"\delta t = " + str('%f' %A[6]))
coincidence_frequncy = np.sqrt(A[5]*2*np.pi)*A[4]/4
ax.text(-32, 3.2,"CF = "+str('%f' %coincidence_frequncy ))
rect2=patches.Rectangle((-32, 2.7), 
                        23, 0.4, 
                        fc ='none',  
                        ec ='b', 
                        label="big fit parameters",
                        lw = 1)
ax.add_patch(rect2)      
ax.text(-32, 3.0,"A = "+str('%f' %A[1]))     
ax.text(-32, 2.9,"sigma2 = "+str('%f' %A[2]))  
ax.text(-32, 2.8,"\delta t = "+str('%f' %A[3])) 

ax3 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], 0, 0 ,0, A[4], A[5], A[6]), 'g', label="small_fit", linestyle = "--")

ax4 = plt.scatter(time_intergarl, frequncy, label="experiment_data")
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('Land_8_9')

plt.savefig("figures/Land_8_9.png")




'''
PMT pairs 9 and 11
'''

# get experiment data frequncy
ce9_11 = coincidence_event(np.array(data_timelist_9, dtype = "int64"), np.array(data_timelist_11, dtype = "int64"))

density, b = np.histogram(ce9_11, bins=21, range=(-42, 42))
time_intergarl = np.array([-40., -36., -32., -28., -24., -20., -16., -12.,  -8.,  -4.,   0.,
                           4.,   8.,  12.,  16.,  20.,  24.,  28.,  32.,  36.,  40.])
frequncy = density/ 60 /time_minutes

# fit data

param_bounds = ([0, 1, 100, -25, 1, 1.5, -25], [1000, 5000, 100000, 25, 5000, 500, 25])
A, B = curve_fit(double_gauss_fit, time_intergarl, frequncy, p0=[
                 500, 500, 800,0,100,100,0], bounds=param_bounds)
'''[0.5,2,8,3],'''
fig=plt.figure(dpi=300,figsize=(12,8))
ax = fig.add_subplot(111)
ax1 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], A[1], A[2], A[3], A[4], A[5], A[6]), 'r', label="total_fit")
ax2 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], A[1], A[2], A[3], 0, 0, 0), 'b', label="big_fit", linestyle = "--" )


rect1=patches.Rectangle((-32, 3.1), 
                        23, 0.5, 
                        fc ='none',  
                        ec ='g', 
                        label="small fit parameters",
                        lw = 1)
ax.add_patch(rect1)      
ax.text(-32, 3.5,"A = " + str('%f' %A[4]))     
ax.text(-32, 3.4,"sigma2 = " + str('%f' %A[5]))  
ax.text(-32, 3.3,"\delta t = " + str('%f' %A[6]))
coincidence_frequncy = np.sqrt(A[5]*2*np.pi)*A[4]/4
ax.text(-32, 3.2,"CF = "+str('%f' %coincidence_frequncy ))
rect2=patches.Rectangle((-32, 2.7), 
                        23, 0.4, 
                        fc ='none',  
                        ec ='b', 
                        label="big fit parameters",
                        lw = 1)
ax.add_patch(rect2)      
ax.text(-32, 3.0,"A = "+str('%f' %A[1]))     
ax.text(-32, 2.9,"sigma2 = "+str('%f' %A[2]))  
ax.text(-32, 2.8,"\delta t = "+str('%f' %A[3])) 

ax3 = plt.plot(np.linspace(-40, 40, 100), double_gauss_fit(np.linspace(-40,
              40, 100), A[0], 0, 0 ,0, A[4], A[5], A[6]), 'g', label="small_fit", linestyle = "--")

ax4 = plt.scatter(time_intergarl, frequncy, label="experiment_data")
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('Land_9_11')

plt.savefig("figures/Land_9_11.png")



# plt.plot(dataframe_th["voltage[mV]"][3])
# plt.xlabel("ADC count [4ns]")
# plt.ylabel("voltage [mV]")
# plt.savefig("figures/pluse.png")

time = np.linspace(-40, 40 ,21)
cf = 3.92
sigma = 2.38
deltaT = 4
A = cf * deltaT /(np.sqrt(2 * np.pi) * sigma)
plt.figure(dpi=300,figsize=(12,8))

ax3 = plt.plot(np.linspace(-40, 40, 100), gauss_fit(np.linspace(-40, 40, 100), 0 , A , sigma**2, -4.83) , 'r', label="fit", linestyle = "--")

plt.scatter(time, gauss_fit(time, 0 , A , sigma**2, -4.83), label="simulation + jingping")
plt.xlabel("\delta t [ns]")
plt.ylabel("frequncy [Hz]")
plt.legend(loc='best')
plt.title('Land_18_21')
plt.savefig("figures/SLand_18_21.png")